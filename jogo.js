const botaoPedra = document.getElementById("botaoPedra");
const botaoPapel = document.getElementById("botaoPapel");
const botaoTesoura = document.getElementById("botaoTesoura");
const botoes = document.getElementById("botoes");
const resultado = document.getElementById("resultado");

const pedra = "Pedra"
const papel = "Papel"
const tesoura = "Tesoura"

let escolhaMaquina = ""

botaoPedra.addEventListener("click", function () {

    escolhaMaquina = maquina()
    resultado.innerText = `Eu = Pedra \n Máquina = ${escolhaMaquina}`
   
    if (escolhaMaquina === pedra) {
        let h2 = document.createElement("h2");
        h2.className = "empate";
        h2.innerText = "Empate!";
        resultado.appendChild(h2);
    }
    if (escolhaMaquina === papel) {
        let h2 = document.createElement("h2");
        h2.className = "derrota";
        h2.innerText = "Você perdeu!";
        resultado.appendChild(h2);
    }
    if (escolhaMaquina === tesoura) {
        let h2 = document.createElement("h2");
        h2.className = "vitoria";
        h2.innerText = "Você ganhou!";
        resultado.appendChild(h2);
    }
})

botaoPapel.addEventListener("click", function () {
    
    escolhaMaquina = maquina()
    resultado.innerText = `Eu = Papel \n Máquina = ${escolhaMaquina}`
   
    if (escolhaMaquina === papel) {
        let h2 = document.createElement("h2");
        h2.className = "empate";
        h2.innerText = "Empate!";
        resultado.appendChild(h2);
    }
    if (escolhaMaquina === tesoura) {
        let h2 = document.createElement("h2");
        h2.className = "derrota";
        h2.innerText = "Você perdeu!";
        resultado.appendChild(h2);
    }
    if (escolhaMaquina === pedra) {
        let h2 = document.createElement("h2");
        h2.className = "vitoria";
        h2.innerText = "Você ganhou!";
        resultado.appendChild(h2);
    }
})

botaoTesoura.addEventListener("click", function () {
   
    escolhaMaquina = maquina()
    resultado.innerText = `Eu = Tesoura \n Máquina = ${escolhaMaquina}`
   
    if (escolhaMaquina === tesoura) {
        let h2 = document.createElement("h2");
        h2.className = "empate";
        h2.innerText = "Empate!";
        resultado.appendChild(h2);
    }
    if (escolhaMaquina === pedra) {
        let h2 = document.createElement("h2");
        h2.className = "derrota";
        h2.innerText = "Você perdeu!";
        resultado.appendChild(h2);
    }
    if (escolhaMaquina === papel) {
        let h2 = document.createElement("h2");
        h2.className = "vitoria";
        h2.innerText = "Você ganhou!";
        resultado.appendChild(h2);
    }
})

function maquina () {

    let escolha = "";
    
    let Aleatorio = Math.floor(Math.random() * 3 + 1)
   
    if (Aleatorio === 1) {
        escolha = "Pedra";
    }
    if (Aleatorio === 2) {
        escolha = "Papel";
    }
    if (Aleatorio === 3) {
        escolha = "Tesoura";
    }
    return escolha
}
